#ifndef _FACTORIAL_H__
#define _FACTORIAL_H__

/*
 * compile with (from deps/lib):
 * gcc -shared -o libfactorial.so ../src/factorial.o
 *
 * also make sure to set your environment properly:
 * export LD_LIBRARY_PATH=<full path to libfactorial.so>:$LD_LIBRARY_PATH
 */

extern int factorial(int x);

#endif // _FACTORIAL_H__
