/*
 * compile with (from deps/src):
 * gcc -c -fpic factorial.c
 */

// calculate the factorial of x
int factorial(int x) {
  // 1! = 1
  // 2! = 2 * 1 = 2 * 1!
  // 3! = 3 * 2 * 1 = 3 * 2!
  // 4! = 4 * 3 * 2 * 1 = 4 * 3! 
  if (x == 1) {
    return 1;
  } else {
    return x * factorial(x - 1);
  }
}
