#include <stdlib.h>
#include <stdio.h>

#include <factorial.h>

#include "readwrite.h"

int main() {
  float a = 0;
  float b = 0;
  float c = 0;

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);

  // read my configuration
  read("input", &a, &b, &c);

  printf("a = %f\nb = %f\nc = %f\n", a, b, c);
  printf("hi mom\n");

  // factorial library
  printf("3! = %d\n", factorial(3));
  printf("4! = %d\n", factorial(4));

  return EXIT_SUCCESS;
}
