#include <stdlib.h>
#include <stdio.h>

/* compile with:
 * gcc -o hello-world hello-world.c
 */

// this is the starting point
/* this is a multi-
 * line comment
 */
int main(int argc, char *argv[]) {
  printf("hello world!\n");

  // print hi 10 times
  int i = 0;
  for (i = 0; i < 10; i++) {
    printf("hi number %d\n", i);
  }

  // print the command line args
  printf("There are %d command line arguments.\n", argc);
  printf("They are:\n");
  for (i = 0; i < argc; i++) {
    printf("  %d: %s\n", i, argv[i]);
  }

  // arrays
  int A[10];
  // fill and print
  for (i = 0; i < 10; i++) {
    A[i] = i;
    printf("A[%d] = %d\n", i, A[i]);
  }

  int N = 11;
  int B[N]; // array of length N
  // fill and print
  for (i = 0; i < N; i++) {
    B[i] = i;
    printf("B[%d] = %d\n", i, B[i]);
  }
  
  // error checking
  if (argc == 2) {
    N = atoi(argv[1]);
    int C[N];
    // fill and print
    for (i = 0; i < N; i++) {
      C[i] = i;
      printf("C[%d] = %d\n", i, C[i]);
    }
  } else {
    printf("usage: hello-world N\n");
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}
