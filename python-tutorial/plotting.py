# import modules
import numpy as np
import matplotlib.pyplot as plt

# user numpy to create dummy dataset
N = 100
x = np.zeros(N)
y = np.zeros(N)

for i in range(N):
  x[i] = i * (2. * np.pi / (N-1))
  y[i] = np.sin(x[i])

# plot with matplotlib
plt.plot(x, y)
plt.xlabel('x')
plt.ylabel('sin(x)')
plt.title('y = sin(x)')

plt.axis([0, 2*np.pi, -1, 1])

# save plot to file
fig = plt.gcf()
fig.set_size_inches(5,4)
fig.savefig('plot.png')

# show plot on screen
#plt.show()
