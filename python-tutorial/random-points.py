import random

# open file for writing
fout = open('paraview-points.csv', 'w')

# seed my random number generator
random.seed()

# number of random points
n = 100

# write header
fout.write('x,y,z,value\n')

# generate random numbers
for i in range(n):
  fout.write(str(random.uniform(-1.,1.)) + ',')
  fout.write(str(random.uniform(-1.,1.)) + ',')
  fout.write(str(random.uniform(-1.,1.)) + ',')
  fout.write(str(random.uniform(-1.,1.)) + '\n')

# close file
fout.close()
