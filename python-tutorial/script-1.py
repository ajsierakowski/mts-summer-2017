#!/usr/bin/env python

# numbers
a = 123 + 456
print(a)
b = 1.5 * 4
print(b)
c = 2 * 3
print(c)
d = 2 ** 40
print(d)

# strings
H = 'Hopkins'
print(H)
print(len(H))
print(H[0])
print(H[6])
print(H[-1])
print(H[-2])
print(H[0:3])
print(H[:3])
print(H[3:])
print(H + ' University')
print(H*4)
print(H*c)
print(H.find('pki'))
print(H.find('pi'))
print(H.replace('op', 'ad'))
H = H.replace('op', 'ad')
print(H)
H = H.replace('ad', 'op')
print(H)
JHU = 'Johns ' + H + ' University'
print(JHU)

# booleans

print(True)
f = False
n = None

# lists

L = ['JHU', 1876, None]
print(L[2] == True)
L[2] = True
print(L)
# H[2] = 'P' strings are immutable
print(H)
L.append('Baltimore')
print(L)
L.pop(2)
print(L)
L[1] = 'Homewood'
L.sort()
print(L)

# dictionaries

D = {'school': 'Hopkins', 'nstudents': 12345}
print(D['school'])
print(D['nstudents'])
D['city'] = 'Baltimore'
print(D['city'])
print(D)
D['city'] = ['Baltimore', 'D.C.', 'Annapolis']
print(D)
D[1] = 'test' # sure you can do it, but why?
print(D)

# files

f = open('script-0.py', 'r')
text = f.read() # reads entire file into memory at once
print(text)
f.close()

f = open('script-0.py', 'r')
line = f.readline()  # reads one line at a time; more memory friendly
print(line)
line = f.readline()
print(line)

line = f.readline()
split = line.split('o')
print(split)

f.close()

f = open('script-0.py', 'r')
for line in f:
  print(line)
f.close()
