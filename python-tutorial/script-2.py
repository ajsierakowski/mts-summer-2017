#!/usr/bin/env python

####################
# Define functions #
####################

# define the input query
def get_input():
  return input('Type something: ')

# determine whether this is a single character or a string
def check_length(string):
  if len(string) < 1:
    print('  Empty')
  elif len(string) < 2:
    print('  Character')
  else:
    print('  String')

################
# Begin script #
################

# get user input
user_in = get_input()

# repeat unitl a "q" appears
while user_in.find('q') == -1:
  # check length
  check_length(user_in)

  # print each character
  for i in range(len(user_in)):
    print('    user_in[' + str(i) + '] = ' + user_in[i])

  # print each character using built-in iterator
  for c in user_in:
    print('    ' + c)

  # request another input
  user_in = get_input()

print('Quitting')
